#include <iostream>
#include <vector>

using namespace std;

struct Creature;
struct Game
{
    vector<Creature*> creatures;
};

enum class Skill
{
    attack = 0,
    defense
};

struct SkillQuery
{
    SkillQuery() = delete;

    SkillQuery(Skill& p_value, int p_result)
      : value(p_value)
      , result(p_result)
    {
    }

    Skill value;
    int result;
};

struct Creature
{
protected:
    Game& m_game;
    int m_base_attack;
    int m_base_defense;

public:
    Creature(Game& game, int base_attack, int base_defense)
      : m_game(game)
      , m_base_attack(base_attack)
      , m_base_defense(base_defense)
    {
    }

    virtual int get_attack() = 0;
    virtual int get_defense() = 0;
    virtual void updateQuery(void* source, SkillQuery& sq) = 0;
};

class Goblin : public Creature
{
private:
    int querySkillValue(Skill p_skill)
    {
        // Initialise query about p_skill
        SkillQuery query(p_skill, 0);
        for (auto& creature : m_game.creatures)
        {
            creature->updateQuery(this, query);
        }
        return query.result;
    }

protected:
    void updateQuery(void* p_source, SkillQuery& p_query) override
    {
        // Here we compare p_source with the pointer of the instance calling updateQuery
        // In other words we compare p_source with the current creature of the for loop
        if (p_source == this)
        {
            // In case both pointers are identical, p_source can be a basic Goblin or Goblin King
            // we add either (+1/+1) or (+3/+3) depending on the Goblin nature through polymorphism
            if (p_query.value == Skill::attack)
            {
                p_query.result += m_base_attack;
            }
            else if (p_query.value == Skill::defense)
            {
                p_query.result += m_base_defense;
            }
        }
        else
        {
            // In case pointers are different,
            // ==> p_source can be a goblin or a goblin King
            // ==> this (i.e. the current creature of the for loop) is a simple Goblin
            // We add the +1 defense bonus to the query provided as parameter
            if (p_query.value == Skill::defense)
            {
                p_query.result += 1;
            }
        }
    }

public:
    Goblin(Game& game, int base_attack, int base_defense)
      : Creature(game, base_attack, base_defense)
    {
    }

    Goblin(Game& game)
      : Creature(game, 1, 1)
    {
    }

    int get_attack() override
    {
        return querySkillValue(Skill::attack);
    }

    int get_defense() override
    {
        return querySkillValue(Skill::defense);
    }
};

class GoblinKing : public Goblin
{
public:
    GoblinKing(Game& game)
      : Goblin(game, 3, 3)
    {
    }

private:
    void updateQuery(void* p_source, SkillQuery& p_query) override
    {
        // Here we compare p_source with the pointer of the instance calling updateQuery
        // In other words we compare p_source with the current creature of the for loop
        if (p_source == this)
        {
            // In case both pointers are identical, no bonus is applied to the Goblin King itself
            // We simply invoque Goblin::updateQuery
            Goblin::updateQuery(p_source,
                                p_query); // +3/+3 will be applied to Goblin King attack/defense
        }
        else
        {
            // In case pointers are different,
            // ==> p_source is a goblin (either standard or King)
            // ==> this (i.e. the current creature of the for loop) is a Goblin King
            // We add the +1 attack bonus due to the Goblin King to the Goblin query
            if (p_query.value == Skill::attack)
            {
                p_query.result += 1;
            }
            else if (p_query.value == Skill::defense)
            {
                p_query.result += 1; // also provides a +1 defense bonus as a Goblin
            }
        }
    }
};

int main(int argc, char* argv[])
{
    Game game;

    Goblin goblin01(game);
    Goblin goblin02(game);
    Goblin goblin03(game);

    GoblinKing zabugor(game);

    game.creatures.push_back(&goblin01);
    game.creatures.push_back(&zabugor);
    game.creatures.push_back(&goblin02);
    game.creatures.push_back(&goblin03);

    cout << "goblin01 has(attack/defense) = " << goblin01.get_attack() << " / "
         << goblin01.get_defense() << endl;
    cout << "GoblinKing has(attack/defense) = " << zabugor.get_attack() << " / "
         << zabugor.get_defense() << endl;
    cout << "goblin02 has(attack/defense) = " << goblin02.get_attack() << " / "
         << goblin02.get_defense() << endl;
}
