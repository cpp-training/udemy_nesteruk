#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>

using namespace std;

struct Value;
struct AdditionExpression;
struct MultiplicationExpression;

struct ExpressionVisitor
{
    // visiting methods here :)

    virtual void visit(Value* value) = 0;
    virtual void visit(AdditionExpression* add_expr) = 0;
    virtual void visit(MultiplicationExpression* mult_expr) = 0;
};

struct ExpressionPrinter : ExpressionVisitor
{
    ostringstream oss;
    string str() const
    {
        return oss.str();
    }

    void reset()
    {
        oss.str("");
    }

    virtual void visit(Value* value) override;
    virtual void visit(AdditionExpression* add_expr) override;
    virtual void visit(MultiplicationExpression* add_expr) override;
};

struct Expression
{
    virtual void accept(ExpressionVisitor& ev) = 0;
};

struct Value : Expression
{
    int value;

    Value(int value)
      : value(value)
    {
    }

    void accept(ExpressionVisitor& ev)
    {
        ev.visit(this);
    }
};

struct AdditionExpression : Expression
{
    Expression &lhs, &rhs;

    AdditionExpression(Expression& lhs, Expression& rhs)
      : lhs(lhs)
      , rhs(rhs)
    {
    }

    void accept(ExpressionVisitor& ev)
    {
        ev.visit(this);
    }
};

struct MultiplicationExpression : Expression
{
    Expression &lhs, &rhs;

    MultiplicationExpression(Expression& lhs, Expression& rhs)
      : lhs(lhs)
      , rhs(rhs)
    {
    }

    void accept(ExpressionVisitor& ev)
    {
        ev.visit(this);
    }
};

/**
 * @brief The ExpressionPrinter class
 *
 * You are asked to implement a double-dispatch visitor called ExpressionPrinter
 * for printing different mathematical expressions.
 * The range of expressions covers addition and multiplication -
 * please put round brackets around addition but not around multiplication!
 * Also, please avoid any blank spaces in output.
 *
 * Example:
 * AdditionExpression{Literal{2}, Literal{3}}
 * output: (2+3)
 *
 */
void ExpressionPrinter::visit(Value* p_value)
{
    oss << p_value->value;
}

void ExpressionPrinter::visit(AdditionExpression* p_add)
{
    oss << "(";
    p_add->lhs.accept(*this);
    oss << "+";
    p_add->rhs.accept(*this);
    oss << ")";
}

void ExpressionPrinter::visit(MultiplicationExpression* p_mult)
{
    p_mult->lhs.accept(*this);
    oss << "*";
    p_mult->rhs.accept(*this);
}

int main(int ac, char* av[])
{
    ExpressionPrinter printer;

    Value v2{ 2 };
    Value v3{ 3 };
    AdditionExpression simple{ v2, v3 };

    simple.accept(printer);
    std::cout << printer.str() << std::endl;
    printer.reset();

    Value mu1{ 5 };
    Value mu2{ -6 };
    Value ad1{ 2 };
    Value ad2{ 3 };

    MultiplicationExpression mult01{ mu1, mu2 };
    AdditionExpression add01{ ad1, ad2 };
    AdditionExpression mixed_exp{ add01, mult01 };

    mixed_exp.accept(printer);

    std::cout << printer.str() << std::endl;

    return 0;
}
