#include "dynamic_decorators.h"
#include "shapes.h"

void wrapper()
{
    Circle circle{ 5 };
    cout << circle.str() << endl;

    ColoredShape red_circle{ circle, "red" };
    cout << red_circle.str() << endl;

    // red_circle.resize(); // oops

    TransparentShape red_half_visible_circle{ red_circle, 128 };
    cout << red_half_visible_circle.str() << endl;
}

int main(int argc, char* argv[])
{
    wrapper();

    return 1;
}
