#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person
{
    friend class ResponsiblePerson;
    int age;

public:
    Person(int age)
      : age(age)
    {
    }

    int get_age() const
    {
        return age;
    }
    void set_age(int age)
    {
        this->age = age;
    }

    string drink() const
    {
        return "drinking";
    }
    string drive() const
    {
        return "driving";
    }
    string drink_and_drive() const
    {
        return "driving while drunk";
    }
};

class ResponsiblePerson
{
public:
    ResponsiblePerson(const Person& person)
      : m_person(person)
    {
    }

    friend std::ostream& operator<<(std::ostream& os, const ResponsiblePerson& p_someone)
    {
        os << "=========   Person Proxy   ===========" << std::endl;
        os << "age = " << p_someone.get_age() << std::endl;
        os << "======================================" << std::endl;

        return os;
    }

    int get_age() const
    {
        return m_person.get_age();
    }

    void set_age(int age)
    {
        m_person.set_age(age);
    }

    string drink() const
    {
        string result("drinking");
        if (get_age() < 18)
        {
            result = "too young";
        }
        return result;
    }

    string drive() const
    {
        string result("driving");
        if (get_age() < 16)
        {
            result = "too young";
        }
        return result;
    }

    string drink_and_drive() const
    {
        return "dead";
    }

private:
    Person m_person;
};

int main()
{
    Person guillaume(17);

    ResponsiblePerson guil_twin(guillaume);

    string drink_test = guil_twin.drink();
    string drive_test = guil_twin.drive();

    string alcohol_test = guil_twin.drink_and_drive();

    cout << guil_twin;

    cout << "Drinking test: " << drink_test << endl;
    cout << "Drinving test: " << drive_test << endl;
    cout << "Drink and drive test: " << alcohol_test << endl << endl;

    Person palmade(50);

    ResponsiblePerson palmade_twin(palmade);

    drink_test = palmade_twin.drink();
    drive_test = palmade_twin.drive();

    alcohol_test = palmade_twin.drink_and_drive();

    cout << "\n" << palmade_twin;

    cout << "Drinking test: " << drink_test << endl;
    cout << "Drinving test: " << drive_test << endl;
    cout << "Drink and drive test: " << alcohol_test << endl;
}
