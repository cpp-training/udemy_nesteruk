#include <cmath>
#include <complex>
#include <iostream>
#include <memory>
#include <tuple>
#include <vector>

using namespace std;

struct DiscriminantStrategy
{
    virtual double calculate_discriminant(double a, double b, double c) = 0;
};

/**
 * @brief The OrdinaryDiscriminantStrategy class
 * In OrdinaryDiscriminantStrategy , If the discriminant is negative,
 * we return it as-is. This is OK,
 * since our main API returns std::complex  numbers anyway.
 *
 */
struct OrdinaryDiscriminantStrategy : DiscriminantStrategy
{
    double calculate_discriminant(double a, double b, double c) override
    {
        return b * b - 4. * a * c;
    }
};

/**
 * @brief The RealDiscriminantStrategy class
 * In RealDiscriminantStrategy , if the discriminant is negative,
 * the return value is NaN (not a number). NaN propagates throughout
 * the calculation, so the equation solver gives two NaN values.
 *
 */
struct RealDiscriminantStrategy : DiscriminantStrategy
{
    double calculate_discriminant(double a, double b, double c) override
    {
        double discriminant = b * b - 4. * a * c;

        if (discriminant < 0.)
        {
            return std::nan("");
        }
        return discriminant;
    }
};

class QuadraticEquationSolver
{
    DiscriminantStrategy& strategy;

public:
    QuadraticEquationSolver(DiscriminantStrategy& strategy)
      : strategy(strategy)
    {
    }

    tuple<complex<double>, complex<double>> solve(double a, double b, double c)
    {
        double discriminant = strategy.calculate_discriminant(a, b, c);

        tuple<complex<double>, complex<double>> solutions;
        if (std::isnan(discriminant))
        {
            solutions = std::make_tuple(std::complex<double>(std::nan(""), std::nan("")),
                                        std::complex<double>(std::nan(""), std::nan("")));
        }
        else if (discriminant == 0.)
        {
            double real = -b / (2 * a);
            double imag = 0.;
            solutions
              = std::make_tuple(std::complex<double>(real, imag), std::complex<double>(real, imag));
        }
        else if (discriminant > 0.)
        {
            double real_p1 = -b / (2 * a);
            double real_p2 = std::sqrt(discriminant) / (2 * a);

            solutions = std::make_tuple(std::complex<double>(real_p1 + real_p2, 0.),
                                        std::complex<double>(real_p1 - real_p2, 0.));
        }
        else if (discriminant < 0.)
        {
            double real = -b / (2 * a);
            double imag = std::sqrt(-discriminant) / (2 * a);

            solutions = std::make_tuple(std::complex<double>(real, imag),
                                        std::complex<double>(real, -imag));
        }

        return solutions;
    }
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::complex<T>& c)
{
    os << "(" << c.real() << ", " << c.imag() << ")";
    return os;
}

template<typename T1, typename T2>
std::ostream& operator<<(std::ostream& os, const std::tuple<T1, T2>& t)
{
    os << "(" << std::get<0>(t) << ", " << std::get<1>(t) << ")";
    return os;
}

int main(int argc, char* argv[])
{
    std::unique_ptr<DiscriminantStrategy> ord_strat
      = std::make_unique<OrdinaryDiscriminantStrategy>();

    QuadraticEquationSolver eq_solver(*ord_strat);

    // p_1(x) = 2x^2 - 15x +7
    auto solutions = eq_solver.solve(2., -15., 7.);
    std::cout << "p_1(x) = 2x^2 - 15x +7" << std::endl;
    std::cout << solutions << std::endl;

    // p_2(x) = x^2 + 4
    solutions = eq_solver.solve(1., 0., 4.);
    std::cout << "p_2(x) = x^2 + 4" << std::endl;
    std::cout << solutions << std::endl;

    // getchar();
    return 0;
}
