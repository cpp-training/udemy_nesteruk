#pragma once 

#include <iostream>
#include <string>
#include <vector>

#include "flowers.h"

using namespace std;



// RedFlower follows
// decorator design pattern
class RedFlower : public Flower
{
public:
    RedFlower(Flower& p_flower)
      : m_flower(p_flower)
    {
    }

    string str() override
    {
        string output = m_flower.str();
        if (!hasAColor(output))
            output += " that is ";

        if (!isAlreadyRed(output) && isAlreadyBlue(output))
        {
            output += " and red";
        }
        else if (!isAlreadyRed(output))
        {
            output += "red";
        }

        return output;
    }

private:
    Flower& m_flower;
};

// BlueFlower follows
// decorator design pattern
class BlueFlower : public Flower
{
public:
    BlueFlower(Flower& p_flower)
      : m_flower(p_flower)
    {
    }

    string str() override
    {
        string output = m_flower.str();
        if (!hasAColor(output))
            output += " that is ";

        if (!isAlreadyBlue(output) && isAlreadyRed(output))
        {
            output += " and blue";
        }
        else if (!isAlreadyBlue(output))
        {
            output += "blue";
        }

        return output;
    }

private:
    Flower& m_flower;
};


