#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class IRat
{
public:
    virtual void notify() = 0;
};

class Game
{
public:
    vector<IRat*> observers;

    void add_rat(IRat* obs)
    {
        observers.push_back(obs);
    }

    void remove_rat(IRat* obs)
    {
        auto it = find(observers.begin(), observers.end(), obs);
        if (it != observers.end())
        {
            observers.erase(it);
        }
    }

    void notify_observers()
    {
        for (auto obs : observers)
        {
            obs->notify();
        }
    }
};

class Rat : public IRat
{
public:
    Game& game;
    int attack;

    Rat(Game& game)
      : game(game)
      , attack(1)
    {
        game.add_rat(this);
        notify_observers();
    }

    ~Rat()
    {
        game.remove_rat(this);
        notify_observers();
    }

    void notify()
    {
        attack = game.observers.size();
    }

    void notify_observers()
    {
        game.notify_observers();
    }
};

int main()
{
    Game game;

    Rat rat1(game);
    Rat rat2(game);

    cout << rat1.attack << endl; // should print 2
    cout << rat2.attack << endl; // should print 2

    Rat rat3(game);

    cout << rat1.attack << endl; // should print 3
    cout << rat2.attack << endl; // should print 3
    cout << rat3.attack << endl; // should print 3

    return 0;
}
