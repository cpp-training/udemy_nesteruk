#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Value
{
public:
    virtual int get_sum() = 0;
};

class SingleValue : public Value
{
public:
    SingleValue(int value)
      : value(value)
    {
    }

    int get_sum() override
    {
        return value;
    }

private:
    int value;
};

class ManyValues : public Value
{
public:
    ManyValues() = default;

    int get_sum() override
    {
        int sum = 0;
        for (const auto& value : values)
        {
            sum += value;
        }
        return sum;
    }
    void add(int value)
    {
        values.push_back(value);
    }

private:
    std::vector<int> values;
};

int sum(const vector<Value*>& items);
