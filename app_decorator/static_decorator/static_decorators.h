#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <cstdint>

#include "shapes.h"

using namespace std;

// mixin inheritance

// note: class, not typename
template<typename T>
struct ColoredShape2 : T
{
    static_assert(is_base_of<Shape, T>::value, "Template argument must be a Shape");

    string color;

    // need this (or not!)
    ColoredShape2()
    {
    }

    template<typename... Args>
    ColoredShape2(const string& color, Args... args)
      : T(std::forward<Args>(args)...)
      , color{ color }
    // you cannot call base class ctor here
    // b/c you have no idea what it is
    {
    }

    string str() const override
    {
        ostringstream oss;
        oss << T::str() << " has the color " << color;
        return oss.str();
    }
};

template<typename T>
struct TransparentShape2 : T
{
    uint8_t transparency;

    template<typename... Args>
    TransparentShape2(const uint8_t transparency, Args... args)
      : T(std::forward<Args>(args)...)
      , transparency{ transparency }
    {
    }

    string str() const override
    {
        ostringstream oss;
        oss << T::str() << " has " << static_cast<float>(transparency) / 255.f * 100.f
            << "% transparency";
        return oss.str();
    }
};
