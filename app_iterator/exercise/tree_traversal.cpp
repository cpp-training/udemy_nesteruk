#include <iostream>
#include <vector>
using namespace std;

template<typename T>
struct Node
{
    T value;
    Node* left{ nullptr };
    Node* right{ nullptr };
    Node* parent{ nullptr };

    Node(T value)
      : value(value)
    {
    }

    Node(T value, Node<T>* left, Node<T>* right)
      : value(value)
      , left(left)
      , right(right)
    {
        left->parent = right->parent = this;
    }

    Node<T>* find_node(Node<T>* root, T value)
    {
        if (root == nullptr)
        {
            return nullptr; // base case: node not found
        }
        if (root->value == value)
        {
            return root; // base case: node found
        }
        // recursively search in the left and right subtrees
        Node<T>* left = find_node(root->left, value);
        Node<T>* right = find_node(root->right, value);
        // return the result of the search
        return left != nullptr ? left : right;
    }

    /**
     * @brief preorder_traversal
     * Traverse the node and its children preorder
     * and put all the results into `result`
     *
     * https://en.wikipedia.org/wiki/Tree_traversal#Pre-order_implementation
     * @param result
     */
    void preorder_traversal(vector<Node<T>*>& result)
    {
        // 1- Visit the current node
        result.push_back(this);

        // 2- Recursively traverse the current node's left subtree
        if (left != nullptr)
        {
            left->preorder_traversal(result);
        }

        // 3- Recursively traverse the current node's right subtree
        if (right != nullptr)
        {
            right->preorder_traversal(result);
        }
    }

    /**
     * @brief postorder_traversal
     * Traverse the node and its children post-order
     * and put all the results into `result`
     *
     * https://en.wikipedia.org/wiki/Tree_traversal#Pre-order_implementation
     * @param result
     */
    void postorder_traversal(vector<Node<T>*>& result)
    {
        // 1- Recursively traverse the current node's left subtree
        if (left != nullptr)
        {
            left->postorder_traversal(result);
        }

        // 2- Recursively traverse the current node's right subtree
        if (right != nullptr)
        {
            right->postorder_traversal(result);
        }

        // 3- Visit the current node
        result.push_back(this);
    }
};

int main(int argc, char const* argv[])
{
    // STEP 1: Build a dummy tree
    // depth 1
    Node<int>* root = new Node<int>(1);

    // depth 2
    Node<int>* leftChild1 = new Node<int>(2);
    Node<int>* rightChild1 = new Node<int>(3);

    // depth 3
    Node<int>* leftChild2 = new Node<int>(4);
    Node<int>* rightChild2 = new Node<int>(5);
    Node<int>* leftChild3 = new Node<int>(6);
    Node<int>* rightChild3 = new Node<int>(7);

    // depth 4
    Node<int>* newLeftChild1 = new Node<int>(8);

    // fill in depth 1 nodes
    root->left = leftChild1;
    root->right = rightChild1;

    leftChild1->parent = rightChild1->parent = root;
    // fill in depth 2 nodes
    leftChild1->left = leftChild2;
    leftChild1->right = rightChild2;
    rightChild1->left = leftChild3;
    rightChild1->right = rightChild3;

    leftChild2->parent = rightChild2->parent = leftChild1;
    leftChild3->parent = rightChild3->parent = rightChild1;
    // fill in depth 3 nodes
    rightChild2->left = newLeftChild1;

    newLeftChild1->parent = rightChild2;

    // STEP 2: Pre-order (NLR) tree traversal
    vector<Node<int>*> result;

    root->preorder_traversal(result);

    // STEP 3: display result
    cout << "===  Pre-order (NLR) tree traversal  ===" << endl;
    for (auto&& node : result)
    {
        cout << "node value: " << node->value << endl;
    }
    // Expected result: 1, 2, 4, 5, 8, 3, 6, 7

    result.clear();
    // STEP 4: Post-order (LRN) tree traversal
    root->postorder_traversal(result);
    cout << "===  Post-order (LRN) tree traversal  ===" << endl;
    for (auto&& node : result)
    {
        cout << "node value: " << node->value << endl;
    }
    // Expected result: 4, 8, 5, 2, 6, 7, 3, 1

    return 0;
}
