#pragma once 

#include <string>
using namespace std;


class Flower
{
public:
    virtual string str() = 0;

protected:
    bool isAlreadyBlue(const string& p_msg)
    {
        return p_msg.find("blue") != string::npos;
    }

    bool isAlreadyRed(const string& p_msg)
    {
        return p_msg.find("red") != string::npos;
    }

    bool hasAColor(const string& p_msg)
    {
        bool l_test = p_msg.find("blue") != string::npos || p_msg.find("red") != string::npos;

        return l_test;
    }

protected:
    string m_specie;
};

class Rose : public Flower
{
public:
    Rose()
    {
        Flower::m_specie = "A rose";
    }

    string str() override
    {
        return m_specie;
    }
};
