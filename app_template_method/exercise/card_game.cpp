#include <complex>
#include <iostream>
#include <tuple>
#include <vector>
using namespace std;

struct Creature
{
    int attack, health;
    std::string name;

    int cur_health;

    Creature(int p_attack, int p_health, const std::string& p_name = "unknown")
      : attack(p_attack)
      , health(p_health)
      , cur_health(p_health)
      , name(p_name)
    {
    }

    void restore()
    {
        cur_health = health;
    }
};

std::ostream& operator<<(std::ostream& os, const Creature& creature)
{
    os << "------ Print Creature ------"
       << "\n"
       << "Name: " << creature.name << "\n"
       << "Attack: " << creature.attack << "\n"
       << "Health: " << creature.health << "\n"
       << "Current Health: " << creature.cur_health << "\n"
       << "----------------------------";

    return os;
}

/**
 * @brief The CardGame class
 *
 * Imagine a typical collectible card game which has cards representing creatures.
 * Each creature has two values: Attack and Health.
 * Creatures can fight each other, dealing their Attack damage,
 * thereby reducing their opponent's health.
 *
 * The class CardGame implements the logic for two creatures fighting one another.
 * However, the exact mechanics of how damage is dealt is different
 *
 */
struct CardGame
{
    vector<Creature> creatures;

    CardGame(const vector<Creature>& creatures)
      : creatures(creatures)
    {
    }

    // return the index of the creature that won (is a live)
    // example:
    // - creature1 alive, creature2 dead, return creature1
    // - creature1 dead, creature2 alive, return creature2
    // - no clear winner: return -1
    int combat(int id1, int id2)
    {
        int status = 0;
        {
            restore(creatures.at(id1), creatures.at(id2));
            hit(creatures.at(id1), creatures.at(id2));
            status = is_there_any_survivor(id1, id2);
        }
        return status;
    }

    int is_there_any_survivor(const int& id1, const int& id2)
    {
        bool creat1_alive = creatures.at(id1).cur_health > 0;
        bool creat2_alive = creatures.at(id2).cur_health > 0;

        int survivor = -1;
        if (!creat1_alive)
        {
            if (creat2_alive)
            {
                survivor = id2;
            }
        }
        else
        {
            if (!creat2_alive)
            {
                survivor = id1;
            }
        }

        return survivor;
    }

    virtual void hit(Creature& attacker, Creature& other) = 0;
    virtual void restore(Creature& attacker, Creature& other) = 0;
};

/**
 * @brief The TemporaryCardDamageGame class
 *
 * TemporaryCardDamage : In some games (e.g., Magic: the Gathering),
 * unless the creature has been killed,
 * its health returns to the original value at the end of combat.
 */
struct TemporaryCardDamageGame : CardGame
{
    TemporaryCardDamageGame(const vector<Creature>& creatures)
      : CardGame(creatures)
    {
    }

    void hit(Creature& attacker, Creature& other) override
    {
        attacker.cur_health -= other.attack;
        other.cur_health -= attacker.attack;
    }

    void restore(Creature& attacker, Creature& other) override
    {
        attacker.cur_health = attacker.health;
        other.cur_health = other.health;
    }
};

/**
 * @brief The PermanentCardDamageGame class
 *
 * PermanentCardDamage : In other games (e.g., Hearthstone)
 * health damage persists.
 *
 */
struct PermanentCardDamageGame : CardGame
{
    PermanentCardDamageGame(const vector<Creature>& creatures)
      : CardGame(creatures)
    {
    }

    void hit(Creature& attacker, Creature& other) override
    {
        attacker.cur_health -= other.attack;
        other.cur_health -= attacker.attack;
    }

    void restore(Creature& attacker, Creature& other) override
    {
        // creature health status are not restored
    }
};

int find_by_name(const std::vector<Creature>& creature_pack, const std::string& name)
{
    for (std::size_t i = 0; i < creature_pack.size(); ++i)
    {
        if (creature_pack[i].name == name)
        {
            return i;
        }
    }
    return -1; // if the creature is not found
}

int main(int argc, char* argv[])
{
    Creature goblin(1, 1, "goblin");
    Creature grizly(2, 3, "grizly");
    Creature shining_knight(2, 5, "shining_knight");
    Creature lazy_snake(1, 6, "lazy_snake");

    vector<Creature> army;
    army.push_back(goblin);
    army.push_back(grizly);
    army.push_back(shining_knight);
    army.push_back(lazy_snake);

    std::cout << "======   NEW GAME   ======" << std::endl;
    TemporaryCardDamageGame magic_party(army);

    std::cout << "***** INCOMING BATTLE *****" << std::endl;
    int survivor = magic_party.combat(find_by_name(army, "grizly"), find_by_name(army, "goblin"));
    auto winner = (survivor != -1 ? army.at(survivor) : Creature(0, 0)).name;
    std::cout << "Creature: " << winner << " (id = " << survivor << ")"
              << " is the winner !" << std::endl;

    // Regenerate creatures
    goblin.restore();
    grizly.restore();

    std::cout << "***** INCOMING BATTLE *****" << std::endl;
    survivor
      = magic_party.combat(find_by_name(army, "goblin"), find_by_name(army, "shining_knight"));
    winner = (survivor != -1 ? army.at(survivor) : Creature(0, 0)).name;
    std::cout << "Creature: " << winner << " (id = " << survivor << ")"
              << " is the winner !" << std::endl;

    // Regenerate creatures
    goblin.restore();
    shining_knight.restore();

    std::cout << shining_knight << std::endl;
    // std::cout << lazy_snake << std::endl;

    std::cout << "======   NEW GAME   ======" << std::endl;

    PermanentCardDamageGame hearthstone_party(army);
    int soldier01 = find_by_name(army, "shining_knight");
    int soldier02 = find_by_name(army, "lazy_snake");

    std::cout << "***** INCOMING BATTLE *****" << std::endl;
    // 1st round of combat
    survivor = hearthstone_party.combat(soldier01, soldier02);
    std::cout << "Creature: " << survivor << " is the winner !" << std::endl;
    // 2nd round of combat
    survivor = hearthstone_party.combat(soldier01, soldier02);
    std::cout << "Creature: " << survivor << " is the winner !" << std::endl;
    // 3rd round of combat
    survivor = hearthstone_party.combat(soldier01, soldier02);
    winner = (survivor != -1 ? army.at(survivor) : Creature(0, 0)).name;
    std::cout << "Creature: \n"
              << winner << " (id = " << survivor << ")"
              << "\n... is the winner !" << std::endl;

    return 0;
}
