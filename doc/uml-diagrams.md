
## Creational Patterns
### Abstract factory
![Abstract Factory UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Abstract_factory_UML.svg/1024px-Abstract_factory_UML.svg.png)

### Builder
![Builder UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Builder_UML_class_diagram.svg/800px-Builder_UML_class_diagram.svg.png)

### Factory
![Factory Method UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/FactoryMethod.svg/1024px-FactoryMethod.svg.png)

### Prototype
![Prototype UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Prototype_UML.svg/800px-Prototype_UML.svg.png)

### Singleton
![Singleton UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Singleton_UML_class_diagram.svg/1024px-Singleton_UML_class_diagram.svg.png)


## Behavioral Patterns
### Chain of Responsibility
![Chain of Responsibility UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Chain_of_Responsibility.svg/1920px-Chain_of_Responsibility.svg.png)

### Command
![Command UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Command_pattern.svg/1024px-Command_pattern.svg.png)

### Interpreter
![Interpreter UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/W3sDesign_Interpreter_Design_Pattern_UML.jpg/1024px-W3sDesign_Interpreter_Design_Pattern_UML.jpg)

### Iterator
![Iterator UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Iterator_UML_class_diagram.svg/1200px-Iterator_UML_class_diagram.svg.png)


### Mediator
![Mediator UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Mediator_design_pattern.png/640px-Mediator_design_pattern.png)

### Memento
![Memento UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/W3sDesign_Memento_Design_Pattern_UML.jpg/1024px-W3sDesign_Memento_Design_Pattern_UML.jpg)

### Observer
![Observer UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Observer.svg/1280px-Observer.svg.png)

### State
![State UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/W3sDesign_State_Design_Pattern_UML.jpg/1024px-W3sDesign_State_Design_Pattern_UML.jpg)

### Strategy
![Strategy UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Strategy_Pattern_in_UML.png/800px-Strategy_Pattern_in_UML.png)

### Template method
![Template Method UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Template_Method_UML.png/640px-Template_Method_UML.png)

### Visitor
![Visitor UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Visitor_design_pattern.svg/585px-Visitor_design_pattern.svg.png)


## Structural Patterns
### Adapter
![Adapter UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/W3sDesign_Adapter_Design_Pattern_UML.jpg/1024px-W3sDesign_Adapter_Design_Pattern_UML.jpg)

### Bridge
![Bridge UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Bridge_UML_class_diagram.svg/800px-Bridge_UML_class_diagram.svg.png)

### Composite
![Composite UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Composite_UML_class_diagram_%28fixed%29.svg/1280px-Composite_UML_class_diagram_%28fixed%29.svg.png)

### Decorator
![Decorator UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Decorator_UML_class_diagram.svg/800px-Decorator_UML_class_diagram.svg.png)

### Facade
![Facade UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Facade_design_pattern.png/480px-Facade_design_pattern.png)

### Flyweight
![Flyweight UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flyweight_pattern_UML.svg/800px-Flyweight_pattern_UML.svg.png)

### Proxy
![Proxy UML diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Proxy_pattern_diagram.svg/1200px-Proxy_pattern_diagram.svg.png)


blabla ...