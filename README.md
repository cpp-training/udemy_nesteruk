## Design patterns course by Dmitri Nesteruk

## Table of contents
<!-- vim-markdown-toc GitLab -->

* [How to use](#how-to-use)
    * [Build](#build)
    * [Create Conan package](#create-conan-package)
* [Introduction to Design Patterns](#introduction-to-design-patterns)
    * [Creational Patterns](#creational-patterns)
    * [Behavioral Patterns](#behavioral-patterns)
    * [Structural Patterns](#structural-patterns)

<!-- vim-markdown-toc -->

## How to use

### Build

To build this project you must have `conan`, `cmake` and a compiler that support cpp17.

```bash
git clone https://gitlab.com/cpp-training/udemy_nesteruk
mkdir build
cd build
conan install ../udemy_nesteruk -s compiler.cppstd=17 -b missing
cmake ../udemy_nesteruk
make -j8
ctest
```

### Create Conan package

Simply run the following command 
```bash
conan create . -s compiler.cppstd=17 -b missing
```
in the folder containing the Conan file.


## Introduction to Design Patterns

Design patterns are general solutions to common programming problems that have been discovered and refined over time by experienced software developers. They provide a standard language and set of practices for solving problems in software design, making it easier for developers to communicate and collaborate with each other. 

There are three main categories of design patterns: 

1. Creational patterns - 
These patterns are concerned with the process of object creation and deal with ways to create objects without directly specifying their class.

2. Behavioral patterns - 
These patterns are focused on communication between objects and classes, and how they interact with each other.

3. Structural patterns - 
These patterns are concerned with object composition and provide ways to organize objects to form larger structures.

Here is a list of all design patterns with their UML diagram and brief description:

### Creational Patterns

1. [Abstract Factory Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Abstract_factory_UML.svg/1024px-Abstract_factory_UML.svg.png) - 
Creates objects of related classes without specifying the exact class to create.

2. [Builder Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Builder_UML_class_diagram.svg/800px-Builder_UML_class_diagram.svg.png) - 
Separates the construction of a complex object from its representation so that the same construction process can create different representations.

3. [Factory Method Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/FactoryMethod.svg/1024px-FactoryMethod.svg.png) - 
Creates objects without specifying the exact class to create, using a method that returns a new object.

4. [Prototype Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Prototype_UML.svg/800px-Prototype_UML.svg.png) - 
Creates new objects by copying an existing object.

5. [Singleton Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Singleton_UML_class_diagram.svg/1024px-Singleton_UML_class_diagram.svg.png) - 
Ensures that a class has only one instance, and provides a global point of access to that instance.

### Behavioral Patterns

6. [Chain of Responsibility Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Chain_of_Responsibility.svg/1920px-Chain_of_Responsibility.svg.png) - 
Passes a request through a chain of handlers until it is handled by one of them.

7. [Command Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Command_pattern.svg/1024px-Command_pattern.svg.png) - 
Encapsulates a request as an object, allowing it to be parameterized and queued for execution.

8. [Interpreter Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/W3sDesign_Interpreter_Design_Pattern_UML.jpg/1024px-W3sDesign_Interpreter_Design_Pattern_UML.jpg) - 
Defines a grammar for a language and a way to interpret sentences in that language.

9. [Iterator Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Iterator_UML_class_diagram.svg/1200px-Iterator_UML_class_diagram.svg.png) - 
Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

10. [Mediator Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Mediator_design_pattern.png/640px-Mediator_design_pattern.png) - 
Defines an object that encapsulates how a set of objects interact, promoting loose coupling.

11. [Memento Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/W3sDesign_Memento_Design_Pattern_UML.jpg/1024px-W3sDesign_Memento_Design_Pattern_UML.jpg) - 
Captures and externalizes an object's internal state, so that it can be restored later.

12. [Observer Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Observer.svg/1280px-Observer.svg.png) - 
Defines a one-to-many dependency between objects, so that when one object changes state, all its dependents are notified and updated automatically.

13. [State Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/W3sDesign_State_Design_Pattern_UML.jpg/1024px-W3sDesign_State_Design_Pattern_UML.jpg) - 
Allows an object to alter its behavior when its internal state changes.

14. [Strategy Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Strategy_Pattern_in_UML.png/800px-Strategy_Pattern_in_UML.png) - 
Defines a family of algorithms, encapsulates each one, and makes them interchangeable.

15. [Template Method Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Template_Method_UML.png/640px-Template_Method_UML.png) - 
Defines the skeleton of an algorithm in a method, deferring some steps to subclasses.

16. [Visitor Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Visitor_design_pattern.svg/585px-Visitor_design_pattern.svg.png) - 
Separates an algorithm from an object structure on which it operates.


### Structural Patterns

17. [Adapter Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/W3sDesign_Adapter_Design_Pattern_UML.jpg/1024px-W3sDesign_Adapter_Design_Pattern_UML.jpg) - 
Converts the interface of a class into another interface that clients expect.

18. [Bridge Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Bridge_UML_class_diagram.svg/800px-Bridge_UML_class_diagram.svg.png) - 
Separates an object's abstraction from its implementation, allowing both to vary independently.

19. [Composite Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Composite_UML_class_diagram_%28fixed%29.svg/1280px-Composite_UML_class_diagram_%28fixed%29.svg.png) - 
Composes objects into tree structures to represent part-whole hierarchies.

20. [Decorator Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Decorator_UML_class_diagram.svg/800px-Decorator_UML_class_diagram.svg.png) - 
Attaches additional responsibilities to an object dynamically.

21. [Facade Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Facade_design_pattern.png/480px-Facade_design_pattern.png) - 
Provides a unified interface to a set of interfaces in a subsystem.

22. [Flyweight Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flyweight_pattern_UML.svg/800px-Flyweight_pattern_UML.svg.png) - 
Reduces the memory footprint of objects by sharing common state between them.

23. [Proxy Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Proxy_pattern_diagram.svg/1200px-Proxy_pattern_diagram.svg.png) - 
Provides a surrogate or placeholder for another object to control access to it.

These patterns provide proven solutions to common software development problems and have been widely adopted by developers worldwide.


## UML diagrams

[Please follow this link](doc/uml-diagrams.md)

