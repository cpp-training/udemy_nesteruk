#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

using namespace std;

struct Sentence
{

    // nested structure WordToken
    struct WordToken
    {
        bool capitalize;
        string word;

        WordToken() = delete;

        WordToken(bool p_cap, const string& p_word)
          : capitalize(p_cap)
          , word(p_word)
        {
        }

        void reset()
        {
            capitalize = false;
        }
    };

    // Members
    std::vector<WordToken> tokens_;

    // ctor
    Sentence(const string& text)
    {
        // Split the input string into individual words
        istringstream iss(text);

        string word;
        while (iss >> word)
        {
            tokens_.push_back(WordToken(false, word));
        }
    }

    void reset()
    {
        for (auto&& elem : tokens_)
        {
            elem.reset();
        }
    }

    WordToken& operator[](size_t index)
    {
        // Make sure that the index is valid
        if (index >= tokens_.size())
            throw out_of_range("Index out of range");

        // Return the WordToken at the specified index
        return tokens_[index];
    }

    std::string capitalize_word(const std::string& word) const
    {
        std::string capitalized_word = word;
        if (!capitalized_word.empty())
        {
            // Capitalize the first character of the word
            std::for_each(capitalized_word.begin(), capitalized_word.end() + 1, [](char& c) {
                c = std::toupper(c);
            });
        }

        return capitalized_word;
    }

    string str() const
    {
        string result;
        for (size_t i = 0; i < tokens_.size(); ++i)
        {
            // Capitalize the word if needed
            string word
              = tokens_[i].capitalize ? capitalize_word(tokens_[i].word) : tokens_[i].word;

            // Add the word to the result
            result += word;

            // Add a space after the word (except for the last word)
            if (i < tokens_.size() - 1)
                result += " ";
        }

        return result;
    }
};

template<typename T, T... Is>
void print_sequence(const std::integer_sequence<T, Is...>&)
{
    std::apply([](auto... args) { ((std::cout << args << ' '), ...); }, std::make_tuple(Is...));
}

template<typename T, T... Is>
void capitalize_words_in_sequence(Sentence& sentence, const std::integer_sequence<T, Is...>& seq)
{
    ((sentence[Is].capitalize = true), ...);
}

int main()
{
    Sentence sentence(
      "Four score and seven years ago our fathers brought forth on this continent,\n "
      "a new nation, conceived in Liberty, \nand dedicated to the proposition "
      "that all men are created equal\n");

    std::integer_sequence<unsigned, 1, 2, 5, 6, 9> my_seq{};
    print_sequence(my_seq);
    cout << endl;
    capitalize_words_in_sequence(sentence, my_seq);
    cout << sentence.str() << endl;

    cout << "=====>   RESET   <======" << endl;
    sentence.reset();
    cout << sentence.str();
    cout << endl << endl;

    std::integer_sequence<unsigned, 9, 2, 5, 1, 9, 1, 6> my_seq_dup{};
    print_sequence(my_seq_dup);
    cout << endl;
    capitalize_words_in_sequence(sentence, my_seq_dup);
    cout << sentence.str();
}
