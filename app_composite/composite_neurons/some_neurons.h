#pragma once


struct Neuron;

// CRTP: The Curiously Recurring Template Pattern 
// https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
template<typename Self>
struct SomeNeurons
{
    template<typename T>
    void connect_to(T& other)
    {
        for (Neuron& from : *static_cast<Self*>(this))
        {
            for (Neuron& to : other)
            {
                from.out.push_back(&to);
                to.in.push_back(&from);
            }
        }
    }
};
