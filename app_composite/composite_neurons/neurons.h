#pragma once

#include <iostream>
#include <vector>

#include "some_neurons.h"


struct Neuron : SomeNeurons<Neuron>
{
    std::vector<Neuron*> in, out;
    unsigned int id;

    Neuron()
    {
        static int id = 1;
        this->id = id++;
    }

    /*template<typename T>
    void connect_to(T& other)
    {
        for (Neuron& target : other)
            connect_to(target);
    }*/

    // legal in MSVC only
    /*template<> void connect_to<Neuron>(Neuron& other)
    {
      out.push_back(&other);
      other.in.push_back(this);
    }*/

    // connect_to(vector<Neuron>&)

    Neuron* begin()
    {
        return this;
    }
    Neuron* end()
    {
        return this + 1;
    }

    friend std::ostream& operator<<(std::ostream& os, const Neuron& obj)
    {
        for (Neuron* n : obj.in)
        {
            os << n->id << "\t-->\t[" << obj.id << "]" << std::endl;
        }

        for (Neuron* n : obj.out)
        {
            os << "[" << obj.id << "]\t-->\t" << n->id << std::endl;
        }
        return os;
    }
};
