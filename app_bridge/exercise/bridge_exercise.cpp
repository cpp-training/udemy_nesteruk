#include <iostream>
#include <string>

using namespace std;

struct Renderer
{
    virtual string what_to_render_as(const string& p_message) const = 0;
};

struct VectorRenderer : Renderer
{
    VectorRenderer() = default;

    virtual string what_to_render_as(const string& p_message) const override
    {
        return "Drawing " + p_message + " as lines";
    }
};

struct RasterRenderer : Renderer
{
    RasterRenderer() = default;

    virtual string what_to_render_as(const string& p_message) const override
    {
        return "Drawing " + p_message + " as pixels";
    }
};

struct Shape
{
    Shape(Renderer& p_renderer)
      : m_renderer(p_renderer)
    {
    }

    string str() const
    {
        return m_renderer.what_to_render_as(m_name);
    }

    Renderer& m_renderer;
    string m_name;
};

struct Triangle : Shape
{
    Triangle(Renderer& p_renderer)
      : Shape(p_renderer)
    {
        m_name = "Triangle";
    }
};

struct Square : Shape
{
    Square(Renderer& p_renderer)
      : Shape(p_renderer)
    {
        m_name = "Square";
    }
};

int main(int argc, char const* argv[])
{
    VectorRenderer dummy_vecr;
    RasterRenderer dummy_raster;

    Triangle tri01(dummy_vecr);
    Triangle tri02(dummy_raster);

    cout << tri01.str() << endl;
    cout << tri02.str() << endl;

    Square squ01(dummy_vecr);
    Square squ02(dummy_raster);

    cout << squ01.str() << endl;
    cout << squ02.str() << endl;

    return 1;
}
