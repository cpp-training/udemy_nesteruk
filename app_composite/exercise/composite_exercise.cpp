#include "composite_exercise.h"

int sum(const std::vector<Value*>& values)
{
    int total = 0;

    for (const auto& value : values)
    {
        total += value->get_sum();
    }
    return total;
}

int main(int ac, char* av[])
{
    SingleValue single_value{ 11 };

    ManyValues other_values;
    other_values.add(22);
    other_values.add(33);

    int total = sum({ &single_value, &other_values });

    cout << "total = " << total << endl;
}
