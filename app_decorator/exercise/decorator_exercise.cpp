#include <iostream>
#include <string>
#include <vector>

#include "decorated_flowers.h"
#include "flowers.h"

using namespace std;

int main()
{
    Rose rose;
    RedFlower red_rose{ rose };
    RedFlower red_red_rose{ red_rose };

    BlueFlower just_blue{ rose };
    BlueFlower blue_red_rose{ red_rose };
    BlueFlower blue_blue_red_rose{ blue_red_rose };

    cout << rose.str() << endl;         // "A rose"
    cout << red_rose.str() << endl;     // "A rose that is red"
    cout << red_red_rose.str() << endl; // "A rose that is red"

    cout << just_blue.str() << endl;     // "A rose that is blue"
    cout << blue_red_rose.str() << endl; // "A rose that is red and blue"
    cout << blue_blue_red_rose.str() << endl;
}
