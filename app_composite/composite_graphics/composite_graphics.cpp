﻿#include <iostream>
#include <memory>
#include <string>
#include <vector>

using namespace std;

struct GraphicObject
{
    virtual void draw() = 0;
};

struct Circle : GraphicObject
{
    Circle(string p_name)
      : m_name(p_name)
    {
    }

    void draw() override
    {
        std::cout << "Circle" << std::endl;
        std::cout << "Name: " << m_name << std::endl;
    }

    string m_name;
};

struct Group : GraphicObject
{
    std::string name;

    explicit Group(const std::string& name)
      : name{ name }
    {
    }

    void draw() override
    {
        std::cout << "Group " << name.c_str() << " contains:" << std::endl;
        for (auto&& o : objects)
            o->draw();
    }

    std::vector<GraphicObject*> objects;
};

int main(int ac, char* av[])
{
    Group root("root");
    Circle c1("Katia"), c2("Marie"), c3("Magda"), c4("Catherine");
    root.objects.push_back(&c1);

    Group subgroup("sub");
    subgroup.objects.push_back(&c2);
    subgroup.objects.push_back(&c3);
    subgroup.objects.push_back(&c4);

    root.objects.push_back(&subgroup);

    root.draw();
}
