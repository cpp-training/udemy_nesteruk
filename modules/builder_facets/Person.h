#pragma once
#include <iostream>
#include <string>

class PersonBuilder;

class Person
{
    // address
    std::string street_address, post_code, city;

    // employment
    std::string company_name, position;
    int annual_income = 0;

    Person()
    {
        std::cout << "Person created\n";
    }

public:
    ~Person()
    {
        std::cout << "Person destroyed\n";
    }

    static PersonBuilder create();

    Person(Person&& other)
      : street_address{ std::move(other.street_address) }
      , post_code{ std::move(other.post_code) }
      , city{ std::move(other.city) }
      , company_name{ std::move(other.company_name) }
      , position{ std::move(other.position) }
      , annual_income{ other.annual_income }
    {
    }

    Person& operator=(Person&& other)
    {
        if (this == &other)
            return *this;
        street_address = std::move(other.street_address);
        post_code = std::move(other.post_code);
        city = std::move(other.city);
        company_name = std::move(other.company_name);
        position = std::move(other.position);
        annual_income = other.annual_income;
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, const Person& obj)
    {
        return os << "street_address: " << obj.street_address << std::endl
                  << " post_code: " << obj.post_code << std::endl
                  << " city: " << obj.city << std::endl
                  << " company_name: " << obj.company_name << std::endl
                  << " position: " << obj.position << std::endl
                  << " annual_income: " << obj.annual_income;
    }

    friend class PersonBuilder;
    friend class PersonAddressBuilder;
    friend class PersonJobBuilder;
};
