#pragma once

#include <iostream>
#include <vector>

#include "some_neurons.h"


struct NeuronLayer
  : std::vector<Neuron>
  , SomeNeurons<NeuronLayer>
{
    NeuronLayer(int count)
    {
        while (count-- > 0)
            emplace_back(Neuron{});
    }

    friend std::ostream& operator<<(std::ostream& os, NeuronLayer& obj)
    {

        for (auto& n : obj)
            os << n;
        return os;
    }
};
