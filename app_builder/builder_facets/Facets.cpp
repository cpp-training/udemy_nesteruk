#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <builder_facets/Person.h>
#include <builder_facets/PersonAddressBuilder.h>
#include <builder_facets/PersonBuilder.h>
#include <builder_facets/PersonJobBuilder.h>

int main()
{
    Person p = Person::create()
                 .lives()
                 .at("123 London Road")
                 .with_postcode("SW1 1GB")
                 .in("London")
                 .works()
                 .at("PragmaSoft")
                 .as_a("Consultant")
                 .earning(10e6);

    std::cout << p << std::endl;
    getchar();

    return 0;
}
