#include <iostream>

#include "Observable.h"
#include "Observer.h"

using namespace std;

class Person : public Observable<Person>
{
    int age{ 0 };

public:
    Person()
    {
    }
    Person(int age)
      : age(age)
    {
    }

    int get_age() const
    {
        return age;
    }

    void set_age(int age)
    {
        if (this->age == age)
            return;

        auto old_can_vote = get_can_vote();
        this->age = age;
        notify(*this, "age");

        if (old_can_vote != get_can_vote())
            notify(*this, "can_vote");
    }

    bool get_can_vote() const
    {
        return age >= 16;
    }
};

// observer & observable

struct ConsolePersonObserver : public Observer<Person>
{
private:
    void field_changed(Person& source, const std::string& field_name) override
    {
        cout << "Person's " << field_name << " has changed to ";

        if (field_name == "age")
            cout << source.get_age();

        if (field_name == "can_vote")
            cout << boolalpha << source.get_can_vote();
        cout << ".\n";
    }
};

int main(int ac, char* av[])
{
    Person adrien{ 10 };

    ConsolePersonObserver cpo;
    adrien.subscribe(cpo);

    adrien.set_age(11);
    adrien.set_age(12);

    adrien.unsubscribe(cpo);

    adrien.set_age(13);

    return 0;
}
