#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

/**
 * @brief The CombinationLock class
 *
 * A combination lock is a lock that opens after the right digits have been entered.
 * A lock is preprogrammed with a combination (e.g., 12345 ) and the user is expected
 * to enter this combination to unlock the lock.
 * The lock has a status  field that indicates the state of the lock.
 *
 * The rules are:
 * If the lock has just been locked (or at startup), the status is LOCKED.
 * If a digit has been entered, that digit is shown on the screen.
 * As the user enters more digits, they are added to status.
 * If the user has entered the correct sequence of digits, the lock status
 * changes to OPEN. If the user enters an incorrect sequence of digits, the lock status changes to
 * ERROR.
 *
 * Please implement the CombinationLock class to enable this behavior.
 * Be sure to test both correct and incorrect inputs.
 *
 *
 */
class CombinationLock
{
    vector<int> combination;

    vector<int> user_input;

public:
    string status;

    CombinationLock(const vector<int>& combination)
      : combination(combination)
      , status("LOCKED")
    {
    }

    void reset()
    {
        std::cout << "======>  RESET NOW  <======" << std::endl;
        user_input.clear();
        status = "LOCKED";
        std::cout << "===========================" << std::endl;
    }

    void enter_digit(int digit)
    {
        if (status == "LOCKED")
        {
            status.clear();
        }

        if (digit > 9)
        {
            auto vectorized(this->integer_to_vector(digit));
            user_input.insert(user_input.end(), vectorized.begin(), vectorized.end());
        }
        else
        {
            user_input.push_back(digit);
        }

        status.append(std::to_string(digit));

        this->transition();
        std::cout << "Status: " << status << "\n";
    }

    void transition()
    {
        if (vectors_equal(combination, user_input))
        {
            status = "OPEN";
            std::cout << "STRONG BOX is " << status << std::endl;
            std::cout << "Congratulations !" << std::endl;
        }
        else if (user_input.size() >= combination.size())
        {
            status = "ERROR";
        }
    }

private:
    bool vectors_equal(const std::vector<int>& v1, const std::vector<int>& v2)
    {
        if (v1.size() != v2.size())
        {
            return false;
        }
        for (size_t i = 0; i < v1.size(); ++i)
        {
            if (v1[i] != v2[i])
            {
                return false;
            }
        }
        return true;
    }

    std::vector<int> integer_to_vector(int p_int)
    {
        std::vector<int> result;
        while (p_int > 0)
        {
            result.push_back(p_int % 10);
            p_int /= 10;
        }
        std::reverse(result.begin(), result.end());
        return result;
    }
};

int main(int argc, char* argv[])
{
    CombinationLock strong_box(std::vector<int>({ 1, 4, 9, 6, 5 }));

    while (strong_box.status != "OPEN")
    {
        std::cout << "STRONG BOX is " << strong_box.status << std::endl;

        std::string input_str;
        std::cout << "Enter an integer: ";
        std::cin >> input_str;

        std::istringstream input_stream(input_str);
        int input;
        if (!(input_stream >> input) || !input_stream.eof())
        {
            throw std::invalid_argument("Invalid input: not an integer");
        }

        strong_box.enter_digit(input);

        if (strong_box.status.size() > 10)
        {
            // reset
            strong_box.reset();
        }
    }

    return 0;
}
