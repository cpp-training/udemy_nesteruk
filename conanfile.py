from conans import ConanFile, tools, CMake


class DesignPatternsConan(ConanFile):
    name = "design_patterns"
    version = "0.1.0"
    author = "Mathieu Drouin (drouin.mathieu@gmail.com)"
    license = "GPL-3.0-only"
    url = "https://gitlab.com/cpp-training/eigen_tuto"
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto",
    }
    description = (
        "SOLID principles and Design patterns course"
    )
    settings = "os", "compiler", "build_type", "arch"
    requires = ("boost/[^1.81]",)
    options = {"shared": [True, False], "build_tests": [True, False]}
    default_options = {
        "shared": False,
        "build_tests": True,
        "gtest:shared": True,
        "gtest:build_gmock": False,
    }
    generators = "cmake", "virtualrunenv"

    def configure(self):
        tools.check_min_cppstd(self, "17")

    def build_requirements(self):
        if self.options.build_tests:
            self.build_requires("gtest/[^1.10]", force_host_context=True)

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(defs={"DESIGN_PATTERNS_BUILD_TESTS": self.options.build_tests})
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        if tools.get_env("CONAN_RUN_TESTS", True):
            cmake.test()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["design_patterns"]
