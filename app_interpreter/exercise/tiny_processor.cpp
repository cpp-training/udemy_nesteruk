#include <iostream>
#include <map>
#include <string>
#include <vector>

/**
 * @brief The ExpressionProcessor class
 *
 * This is a basic expression processor designed to evaluate
 * numeric expressions with the following constraints:
 * Expressions use integral values (e.g., "13" ),
 * single-letter variables defined in variables ,
 * as well as + and - operators only.
 *
 * There is no need to support braces or any other operations .
 *
 * If a variable is not found in Variables
 * Or if we encounter a variable with >1 letter, e.g. ab, the evaluator returns 0
 *
 * In case of any parsing failure, evaluator returns 0
 *
 *
 */
struct ExpressionProcessor
{
    std::map<char, int> variables;

    int calculate(const std::string& expression)
    {
        int result = 0;
        char op = '+';
        bool found_var = true;

        std::size_t i = 0;
        while (i < expression.size())
        {
            if (isdigit(expression[i]))
            {
                int num = 0;

                // handle integral values with many digits
                while (i < expression.size() && isdigit(expression[i]))
                {
                    num = num * 10 + std::stoi(std::string{ expression[i] });
                    i++;
                }
                result = (op == '+') ? result + num : result - num;
            }
            else if (std::isalpha(expression[i]))
            {
                int var_len = 1;
                while (i + var_len < expression.size() && std::isalpha(expression[i + var_len]))
                {
                    var_len++;
                }

                found_var = false;

                // variables with more than 1 letter are discarded
                // i.e. found_var is false
                if (var_len == 1)
                {
                    char var = expression[i];

                    // otherwise, if variable has 1 letter and exists in map
                    // found_var is set to true
                    if (variables.find(var) != variables.end())
                    {
                        found_var = true;
                        int num = variables[var];
                        result = (op == '+') ? result + num : result - num;
                    }
                }

                i++;
            }
            else if (expression[i] == '+' || expression[i] == '-')
            {
                op = expression[i];
                i++;
            }
            else
            {
                return 0;
            }
        }
        return found_var ? result : 0;
    }
};

/**
 * @brief main
 * Examples:
 * calculate("1+2+3")  should return 6
 * calculate("1+2+xy")  should return 0
 * calculate("10-2-x")  when x=3 is in variables should return 5
 *
 * @return
 */
int main()
{
    ExpressionProcessor processor;

    // Test 1: calculate("1+2+3")
    int result1 = processor.calculate("1+2+3");
    std::cout << "Test 1: calculate(\"1+2+3\") = " << result1 << std::endl; // Expected output: 6

    // Test 2: calculate("1+2+xy")
    int result2 = processor.calculate("1+2+xy");
    std::cout << "Test 2: calculate(\"1+2+xy\") = " << result2 << std::endl; // Expected output: 0

    // Test 3: calculate("10-2-x") when x=3 is in variables
    processor.variables['x'] = 3;
    int result3 = processor.calculate("10-2-x");
    std::cout << "x variable was set to " << processor.variables.at('x') << std::endl;
    std::cout << "Test 3: calculate(\"10-2-x\") = " << result3 << std::endl; // Expected output: 5

    return 0;
}
