#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

struct IParticipant
{
    int value{ 0 };
    std::string name;

    IParticipant(const std::string& p_name)
      : name(p_name)
    {
    }

    virtual void say(int value) = 0;
};

struct Participant;

struct Mediator
{
    std::vector<IParticipant*> participants;

    void broadcast(IParticipant* source, const int value)
    {
        for (auto&& l_part : participants)
        {
            // only other participants have to increase their value
            if (source != l_part)
            {
                l_part->value += value;
            }
        }
    }
};

struct Participant : IParticipant
{
    Mediator& mediator;

    Participant(Mediator& mediator, const std::string& p_name)
      : IParticipant(p_name)
      , mediator(mediator)
    {
        mediator.participants.push_back(this);
    }

    Participant(Mediator& mediator)
      : IParticipant("Unknown")
      , mediator(mediator)
    {
        mediator.participants.push_back(this);
    }

    virtual void say(int value)
    {
        mediator.broadcast(this, value);
    }

    friend std::ostream& operator<<(std::ostream& os, const Participant& p_part)
    {
        os << "Name: " << p_part.name;
        os << " - value = " << p_part.value;

        return os;
    }
};

int main(int argc, char* argv[])
{
    Mediator chat_room;

    Participant john(chat_room, "john");
    Participant emilie(chat_room, "emilie");
    Participant mathieu(chat_room, "mathieu");
    Participant dimitri(chat_room, "dimitri");

    john.say(5);

    std::cout << john << std::endl;
    std::cout << emilie << std::endl;
    std::cout << mathieu << std::endl;
    std::cout << dimitri << std::endl;

    emilie.say(2);

    std::cout << john << std::endl;
    std::cout << emilie << std::endl;
    std::cout << mathieu << std::endl;
    std::cout << dimitri << std::endl;

    return 0;
}
