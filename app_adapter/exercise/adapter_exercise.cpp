#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Square
{
    int side{ 0 };

    explicit Square(const int side)
      : side(side)
    {
    }
};

class Rectangle
{
public:
    Rectangle()
    {
        m_width = 1;
        m_height = 1;
    }

    Rectangle(int p_width, int p_height)
      : m_width(p_width)
      , m_height(p_height)
    {
    }

    virtual int width() const
    {
        return m_width;
    }

    virtual int height() const
    {
        return m_height;
    }

    virtual int area() const
    {
        return width() * height();
    }

    friend std::ostream& operator<<(std::ostream& os, const Rectangle& rectangle)
    {
        os << "=========   Rectangle   ===========" << std::endl;
        os << "width = " << rectangle.width() << std::endl;
        os << "height = " << rectangle.height() << std::endl;

        return os;
    }

private:
    int m_width;
    int m_height;
};

struct SquareToRectangleAdapter : Rectangle
{
    const Square& m_square;

    SquareToRectangleAdapter(const Square& square)
      : m_square(square)
    {
    }

    int width() const override
    {
        return m_square.side;
    }

    int height() const override
    {
        return m_square.side;
    }
};

int main(int argc, char const* argv[])
{
    Square sq{ 11 };
    SquareToRectangleAdapter adapter{ sq };

    cout << "Adapter area = " << adapter.area() << endl;

    return 0;
}
