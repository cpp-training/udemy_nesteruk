import os

# Get the user's home directory
home_dir = os.path.expanduser('~')

# Set the directory to search for files in
my_project = '~/cpp_training/patterns_nesteruk/udemy_nesteruk'
root_dir = os.path.join(home_dir, my_project)

print(f"root_dir: {root_dir}")

# Set the command to execute on each file
command = 'clang-format -i -style=file {}'

# Recursively search for all files in the directory and its subdirectories
for dirpath, dirnames, filenames in os.walk(".", topdown=False):
    # print(f"dirpath: {dirpath}")
    # print(f"dirnames: {dirnames}")
    # print(f"filenames: {filenames}")
    for filename in filenames:
        # Check if the file has a C or C++ file extension
        if filename.endswith('.cpp') or filename.endswith('.h'):
            # Get the full path to the file
            file_path = os.path.join(dirpath, filename)

            # Execute the command on the file
            print(f"Apply command: {command.format(file_path)}")
            os.system(command.format(file_path))


